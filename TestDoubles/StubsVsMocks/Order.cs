﻿namespace StubsVsMocks
{
    public class Order
    {
        public Order(string product, int quantity)
        {
            Product = product;
            Quantity = quantity;
        }

        public string Product { get; set; }
        public int Quantity { get; set; }
        public bool IsFilled { get; set; }

        public void Fill(IWarehouse warehouse)
        {
            if (warehouse.HasInventory(Product, Quantity))
            {
                warehouse.Remove(Product, Quantity);
                IsFilled = true;
            }
            else
            {
                IsFilled = false;
            }
        }
    }
}