﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StubsVsMocks
{
    public class WarehouseImpl : IWarehouse
    {
        Dictionary<string, int> inventory = new Dictionary<string, int>();

        public void Add(string productName, int quantity)
        {
            inventory.Add(productName, quantity);
        }

        public int GetInventory(string productName)
        {
            return inventory[productName];
        }

        public void Remove(string productName, int quantity)
        {
            inventory[productName] -= quantity;
        }

        public bool HasInventory(string productName, int quantity)
        {
            return inventory[productName] >= quantity;
        }
    }


}
