﻿using Moq;
using NUnit.Framework;

namespace StubsVsMocks
{
    [TestFixture]
    public class OrderInteractionTests
    {
        private const string TALISKER = "Talisker";

        [Test]
        public void TestFillingRemovesInventoryIfInStock()
        {
            // Arrange
            Order order = new Order(TALISKER, 50);

            Mock<IWarehouse> mqWarehouse = new Mock<IWarehouse>();
            mqWarehouse.Setup(m => m.HasInventory(TALISKER, 50)).Returns(true);
            
            // Act
            order.Fill(mqWarehouse.Object);

            // Assert
            mqWarehouse.Verify(m => m.Remove(TALISKER, 50), Times.Once());
            Assert.IsTrue(order.IsFilled);
        }

        [Test]
        public void TestFillingDoesNotRemoveIfNotEnoughInStock()
        {
            Order order = new Order(TALISKER, 51);
            Mock<IWarehouse> mqWarehouse = new Mock<IWarehouse>();
            mqWarehouse.Setup(m => m.HasInventory(It.IsAny<string>(), It.IsAny<int>())).Returns(false);
            mqWarehouse.Setup(m => m.Remove(TALISKER, 50));

            // Act
            order.Fill(mqWarehouse.Object);

            // Assert
            Assert.IsFalse(order.IsFilled);
        }
    }
}