﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HandRolledMocks
{   
    interface IDependency
    {
        int GetValue(string s);
        void CallMeFirst();
        int CallMeTwice(string s);
        void CallMeLast();
    }
}
