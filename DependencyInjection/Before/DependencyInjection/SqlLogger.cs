﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DependencyInjection
{
    class SqlLogger
    {
        public void LogMessage(string message)
        {
            Console.WriteLine("Logged in database: " + message);
        }
    }
}   
