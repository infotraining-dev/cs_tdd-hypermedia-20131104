﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjection
{
    class Program
    {
        static void PerformCalculation(Service service, int x, int y)
        {
            try
            {
                int result = service.Calculate(x, y);
                Console.WriteLine("Wynik obliczeń: {0}", result);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Błąd obliczeń.");
            }
        }

        static void Main(string[] args)
        {
            Service service = new Service();

            PerformCalculation(service, 20, 5);
            PerformCalculation(service, 4, 0);
        }
    }
}
