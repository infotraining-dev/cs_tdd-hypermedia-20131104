﻿using System.Configuration;
using Ninject;

namespace BusinessApplication.Core
{
    public class BusinessService
    {
        private IDataAccessComponent _dataAccessComponent;
        private IWebServiceProxy _webServiceProxy;
        private ILoggingComponent _loggingComponent;
        

        public BusinessService(IDataAccessComponent dataAccessComponent, IWebServiceProxy webServiceProxy, ILoggingComponent loggingComponent)
        {
            _dataAccessComponent = dataAccessComponent;
            _webServiceProxy = webServiceProxy;
            _loggingComponent = loggingComponent;
        }
    }


    class Program
    {
        public static void Main()
        {
            #region BeforeDIContainer
            string connectionString = "connStr";
            BusinessService service1 = new BusinessService(
                new DataAccessComponent(connectionString), new WebServiceProxy("http://service.com"), new LoggingComponent(new LoggingDataSink())  );
            #endregion

            #region AfterDIContainer
            StandardKernel kernel = new StandardKernel(new CoreModule());

            BusinessService service2 = kernel.Get<BusinessService>();

            BusinessService service2 = kernel.Get<BusinessService>();

            #endregion

        }
    }
}
