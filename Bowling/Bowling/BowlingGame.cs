﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling
{
    public class BowlingGame
    {
        private int[] _throws = new int[21];
        private int _score;
        private int _currentThrow;

        public int Score
        {
            get
            {
                int frameIndex = 0;
                for (int i = 0; i < 10; ++i)
                {
                    if (IsStrike(frameIndex))
                    {
                        _score += StrikeBonus(frameIndex);
                        frameIndex++;
                    }
                    else if (IsSpare(frameIndex))
                    {
                        _score += SpareBonus(frameIndex);
                        frameIndex += 2;
                    }
                    else
                    {
                        _score += SumOfPins(frameIndex);
                        frameIndex += 2;
                    }
                }

                ; return _score;
            }
        }

        private int StrikeBonus(int frameIndex)
        {
            return 10 + _throws[frameIndex + 1] + _throws[frameIndex + 2];
        }

        private bool IsStrike(int frameIndex)
        {
            return _throws[frameIndex] == 10;
        }

        private int SumOfPins(int frameIndex)
        {
            return _throws[frameIndex] + _throws[frameIndex + 1];
        }

        private bool IsSpare(int frameIndex)
        {
            return _throws[frameIndex] + _throws[frameIndex + 1] == 10;
        }

        private int SpareBonus(int frameIndex)
        {
            return 10 + _throws[frameIndex + 2];
        }

        public void Roll(int pins)
        {
            _throws[_currentThrow++] = pins;
        }
    }
}
