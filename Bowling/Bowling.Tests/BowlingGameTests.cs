﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Bowling.Tests
{
    [TestFixture]
    public class BowlingGameTests
    {
        private BowlingGame _game;

        [SetUp]
        public void CreateGame()
        {
            _game = new BowlingGame();
        }

        [TearDown]
        public void CleanUp()
        {
            _game = null;
        }

        [Test, Category("Important")]
        public void WhenGameStartedScoreShouldBeZero()
        {
            // Assert
            Assert.AreEqual(0, _game.Score);
        }

        [Test, Ignore]
        public void WhenAllThrowsInGutterScoreShouldBeZero()
        {
            RollMany(_game, 20, 0);

            // Assert
            Assert.That(_game.Score, Is.EqualTo(0));
        }

        [Test]
        [TestCase(0, 0)]
        [TestCase(1, 20)]
        [TestCase(3, 60)]
        public void WhenAllThrowsNoMarksScoreShouldBeSumOfPins(int pins, int score)
        {
            RollMany(_game, 20, pins);

            Assert.That(_game.Score, Is.EqualTo(score));
        }

        [Test, Category("Important")]
        public void WhenSpareNextThrowIsCountedTwice()
        {
            RollSpare();

            _game.Roll(3);
            RollMany(_game, 17, 0);

            Assert.That(_game.Score, Is.EqualTo(16));
        }

        [Test]
        public void WhenStrikeTwoNextThrowsAreCountedTwice()
        {
            RollStrike();

            RollMany(_game, 2, 3);
            RollMany(_game, 16, 0);

            Assert.That(_game.Score, Is.EqualTo(22));
        }

        [Test]
        public void WhenPerfectGameScoreShouldBe300()
        {
            for(int i = 0; i < 12; ++i)
                RollStrike();

            Assert.That(_game.Score, Is.EqualTo(300));
        }

        private void RollStrike()
        {
            _game.Roll(10); // strike
        }

        private void RollSpare()
        {
            _game.Roll(5);
            _game.Roll(5); 
        }

        private static void RollMany(BowlingGame game,int count, int pins)
        {
            for (int i = 0; i < count; ++i)
                game.Roll(pins);
        }
    }


}
