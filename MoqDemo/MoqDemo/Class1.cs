﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;

namespace MoqDemo
{
    public interface IFoo
    {
        int DoStuff(string id);
    }

    [TestFixture]
    public class MoqDemoTests
    {
        private Mock<IFoo> _mq;

        [SetUp]
        public void Before()
        {
            _mq = new Mock<IFoo>(MockBehavior.Strict);
        }

        [Test]
        public void TestDefaultValueFromMoq()
        {
            _mq.Setup(m => m.DoStuff("1")).Returns(0);
            _mq.Setup(m => m.DoStuff(It.Is<string>(s => s.Length > 3))).Returns(1);

            int result = _mq.Object.DoStuff("232322");

            Assert.AreEqual(0, result);
        }
    }
}
