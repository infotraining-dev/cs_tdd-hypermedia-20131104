﻿using System.Collections.Generic;

namespace ProductService
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetAll();
    }
}