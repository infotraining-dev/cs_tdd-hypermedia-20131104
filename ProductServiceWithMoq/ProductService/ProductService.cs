﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductService
{
    public class ProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public GetProductsResponse GetProducts()
        {
            var response = new GetProductsResponse();

            try
            {
                response.Products = _productRepository.GetAll().OrderBy(p => p.Name);
                response.IsOk = true;
            }
            catch (ApplicationException)
            {
                response.IsOk = false;
                response.ErrorMessage = "Fatal Error";
            }


            return response;
        }
    }
}
