﻿using System.Collections.Generic;

namespace ProductService
{
    public class GetProductsResponse
    {
        public IEnumerable<Product> Products { get; set; }
        public bool IsOk { get; set; }
        public string ErrorMessage { get; set; }
    }
}