﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using ProductService;

namespace ProductService_Tests
{
    [TestFixture]
    public class WhenGettingProducts : WithProductService
    {
        [Test]
        public void ShouldReturnProductsInAlphabeticalOrder()
        {
            // Arrange
            List<Product> products = new List<Product>()
            {
                new Product() {ID = 1, Name = "B"},
                new Product() {ID = 2, Name = "A"},
                new Product() {ID = 3, Name = "D"},
                new Product() {ID = 4, Name = "C"}
            };

            _mqProductRepository.Setup(m => m.GetAll()).Returns(products);

            // Act
            var response = _sut.GetProducts();

            // Assert
            var expectedOrder = products.OrderBy(p => p.Name);

            Assert.IsTrue(response.IsOk);
            CollectionAssert.AreEqual(expectedOrder, response.Products);
        }

        [Test]
        public void ShouldHandleExceptionFromRepository()
        {
            _mqProductRepository.Setup(m => m.GetAll()).Throws<ApplicationException>();

            var response = _sut.GetProducts();

            Assert.That(response.IsOk, Is.False);
            Assert.IsTrue(!string.IsNullOrEmpty(response.ErrorMessage));
        }
    }
}