﻿using System.Collections;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using ProductService;

namespace ProductService_Tests
{

    /*
     *  1. Chcemy wyswietlic liste wszystkich produktów w kolejności alfabetycznej
     *  
     *  2. Chcemy dodać nowy produkt do bazy. 
     *      - Jesli istnieje produkt o takiej samej nazwie:
     *          + nie dodajemy
     *          + zwracamy komunikat o duplikacji
     *  
     *  3. Chcemy usunąć wszystkie wycofane produkty. Wycofane produkty zapisujemy do dziennika logow.  
     */

    public class WithProductService
    {
        protected ProductService.ProductService _sut;
        protected Mock<IProductRepository> _mqProductRepository;

        [SetUp]
        public void BeforeTest()
        {
            _mqProductRepository = new Mock<IProductRepository>();
            _sut = new ProductService.ProductService(_mqProductRepository.Object);
        }
    }
}
