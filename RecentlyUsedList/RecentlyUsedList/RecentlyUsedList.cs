﻿using System;
using System.Collections.Generic;

namespace RecentlyUsedListImpl
{
    //    Develop a recently-used-list class to hold strings 
    //    uniquely in Last-In-First-Out order.

    //    o) The most recently added item is first, the least
    //       recently added item is last.

    //    o) Items can be looked up by index, which counts from zero.

    //    o) Items in the list are unique, so duplicate insertions
    //       are moved rather than added.

    //    o) A recently-used-list is initially empty.

    //    Optional extras

    //    o) Null insertions (empty strings) are not allowed.

    //    o) A bounded capacity can be specified, so there is an upper
    //       limit to the number of items contained, with the least
    //       recently added items dropped on overflow.

    public class RecentlyUsedList : ICollection<string>
    {
        readonly List<string> _list = new List<string>();
        private readonly int _capacity;

        public RecentlyUsedList(int capacity = Int32.MaxValue)
        {
            _capacity = capacity;
        }

        public int Count
        {
            get { return _list.Count; }
        }

        public void Add(string item)
        {
            if (item == null)
                throw new ArgumentNullException("item inserted to the list is null");

            int index = _list.IndexOf(item);

            if (IsDuplicatedItem(index))
                MoveItemToFront(item, index);
            else
                _list.Insert(0, item);

            TrimToDefinedCapacity();
        }

        private void TrimToDefinedCapacity()
        {
            if (_list.Count > _capacity)
                _list.RemoveAt(_list.Count - 1);
        }

        private static bool IsDuplicatedItem(int index)
        {
            return index >= 0;
        }

        private void MoveItemToFront(string item, int index)
        {
            for (int i = index; i > 0; --i)
            {
                _list[i] = _list[i - 1];
            }

            _list[0] = item;
        }

        public string this[int index]
        {
            get { return _list[index]; }
        }

        public void Clear()
        {
            _list.Clear();
        }

        public bool Contains(string item)
        {
            return _list.Contains(item);
        }

        public void CopyTo(string[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(string item)
        {
            return _list.Remove(item);
        }

        public IEnumerator<string> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }
    }
}
