﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecentlyUsedList_Tests
{
    interface IRepository
    {
        void Add();
        void Remove();
    }

    class DbRepository : IRepository
    {
        public void Add()
        {
            Console.WriteLine("Add");
        }

        public void Remove()
        {
            Console.WriteLine("Remove");
        }
    }

    class ActiveDirRepository : IRepository
    {
        public void Add()
        {
            Console.WriteLine("Add");
        }

        public void Remove()
        {
            throw new NotImplementedException();
        }
    }


    class Client
    {
        public void PerformOperation(IRepository repo)
        {

            repo.Remove();
        }

    }

    //
    class Program
    {
        static void Main()
        {
            IRepository repo = new DbRepository();
            Client client = new Client();

            client.PerformOperation(repo);
        }
    }
}
