﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BDD.Calculator.Services;
using BDD.Calculator.UI.Web.Models;

namespace BDD.Calculator.UI.Web.Controllers
{
    public class MathController : Controller
    {
        private readonly IMathService _mathService;

        public MathController(IMathService mathService)
        {
            _mathService = mathService;
        }

        //
        // GET: /Math/

        public ActionResult AddNumbers()
        {
            ViewBag.Title = "Add Numbers";
            return View();
        }

        [HttpPost]
        public ViewResult AddNumbers(AddNumbersViewModel model)
        {
            model.Result = _mathService.Add(model.FirstNumber, model.SecondNumber);

            return View(model);
        }
    }
}
