﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BDD.Calculator.UI.Web.Models
{
    public class AddNumbersViewModel
    {
        public int FirstNumber { get; set; }
        public int SecondNumber { get; set; }
        public int Result { get; set; }
    }
}