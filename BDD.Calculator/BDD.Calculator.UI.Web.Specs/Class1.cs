﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BDD.Calculator.Services;
using BDD.Calculator.UI.Web.Controllers;
using BDD.Calculator.UI.Web.Models;
using Machine.Specifications;
using Machine.Specifications.Mvc;
using Moq;
using It = Machine.Specifications.It;

namespace BDD.Calculator.UI.Web.Specs
{
    public abstract class WithMathController
    {
        protected static MathController _controller;
        protected static Mock<IMathService> _mqMathService; 

        public WithMathController()
        {
            _mqMathService = new Mock<IMathService>();
            _controller = new MathController(_mqMathService.Object);
        }
    }

    

    [Subject(typeof(MathController))]
    public class WhenAddingTwoNumbers : WithMathController
    {
        private static int _firstNumber;
        private static int _secondNumber;
        private static ViewResult _result;

        private Establish context = () =>
        {
            _firstNumber = 10;
            _secondNumber = 20;

            _expectedResult = _firstNumber + _secondNumber;

            _mqMathService.Setup(m => m.Add(_firstNumber, _secondNumber)).Returns(_expectedResult);
        };

        private Because of = () =>
        {
            AddNumbersViewModel vm = new AddNumbersViewModel()
            {
                FirstNumber = _firstNumber,
                SecondNumber = _secondNumber
            };

            _result = _controller.AddNumbers(vm);
        };

        private It should_call_addition_operation_on_service = () =>
        {
            _mqMathService.Verify(m => m.Add(_firstNumber, _secondNumber), Times.Once());
        };

        private It should_return_view_with_model = () =>
        {
            _result.ShouldBeAView();
            _result.ShouldHaveModelOfType<AddNumbersViewModel>();
        };

        private It should_return_correct_result = () =>
        {
            _result.Model<AddNumbersViewModel>().Result.ShouldEqual(_expectedResult);
        };

        private static int _expectedResult;
    }
}
