﻿using System.Web.Mvc;
using BDD.Calculator.Services;
using BDD.Calculator.UI.Web.Controllers;
using BDD.Calculator.UI.Web.Models;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace BDD.Calculator.AcceptanceTests.Steps
{
    [Binding]
    public class AddNumbersSteps
    {
        private AddNumbersViewModel _model;
        private MathController _controller;
        private ViewResult _result;

        [BeforeScenario]
        public void SetUp()
        {
            _controller = new MathController(new MathService());
            _model = new AddNumbersViewModel();
        }

        [Given(@"I have entered (.*) as first number into the calculator")]
        public void GivenIHaveEnteredAsFirstNumberIntoTheCalculator(int firstNumer)
        {
            _model.FirstNumber = firstNumer;
        }
        
        [Given(@"I have entered (.*) as second number into the calculator")]
        public void GivenIHaveEnteredAsSecondNumberIntoTheCalculator(int secondNumber)
        {
            _model.SecondNumber = secondNumber;
        }
        
        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            _result = _controller.AddNumbers(_model);
        }
        
        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(int result)
        {
            Assert.IsInstanceOf<ViewResult>(_result);

            Assert.That((_result.Model as AddNumbersViewModel).Result, Is.EqualTo(result));
        }
    }
}
