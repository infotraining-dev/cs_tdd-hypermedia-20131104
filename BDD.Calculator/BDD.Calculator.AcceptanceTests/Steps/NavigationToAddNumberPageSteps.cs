﻿using System;
using System.Web.Mvc;
using BDD.Calculator.Services;
using BDD.Calculator.UI.Web.Controllers;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace BDD.Calculator.AcceptanceTests.Steps
{
    [Binding]
    public class NavigationToAddNumberPageSteps
    {
        private MathController _controller;
        private ActionResult _result;

        [When(@"a user navigates to AddNumber Page")]
        public void WhenAUserNavigatesToAddNumberPage()
        {
            _controller = new MathController(new MathService());

            _result = _controller.AddNumbers();

        }
        
        [Then(@"a page should be displayed")]
        public void ThenAPageShouldBeDisplayed()
        {
            Assert.IsInstanceOf<ViewResult>(_result);
            Assert.That(_controller.ViewBag.Title, Is.EqualTo("Add Numbers"));
        }
    }
}
