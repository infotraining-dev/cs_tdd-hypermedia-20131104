﻿Feature: AddNumbers
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

Scenario: Navigation to AddNumber Page
	When a user navigates to AddNumber Page
	Then a page should be displayed

@mytag
Scenario: Add two numbers
	Given I have entered 50 as first number into the calculator
	And I have entered 70 as second number into the calculator
	When I press add
	Then the result should be 120 on the screen
